package ggestin.formation.epsi.b3.javaspringboot.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Power {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String name;
    private String description;

    @ManyToMany(mappedBy = "powers")
    private List<SuperHero> superHeroes;

    @ManyToMany(mappedBy = "powers")
    private List<Villain> villains;
}
