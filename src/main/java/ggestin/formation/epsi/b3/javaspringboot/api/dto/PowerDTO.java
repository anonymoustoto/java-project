package ggestin.formation.epsi.b3.javaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PowerDTO {
    private Long id;
    private String name;
    private String description;
}
