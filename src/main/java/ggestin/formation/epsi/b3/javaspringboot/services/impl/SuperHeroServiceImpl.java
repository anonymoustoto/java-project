package ggestin.formation.epsi.b3.javaspringboot.services.impl;

import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;
import ggestin.formation.epsi.b3.javaspringboot.repositories.SuperHeroRepository;
import ggestin.formation.epsi.b3.javaspringboot.services.SuperHeroService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SuperHeroServiceImpl implements SuperHeroService {

    private final SuperHeroRepository superHeroRepository;

    @Override
    public List<SuperHero> getSuperHeroes() {
        return superHeroRepository.findAll();
    }

    @Override
    public Optional<SuperHero> getSuperHeroById(Integer id) {
        return superHeroRepository.findById(id);
    }
}
