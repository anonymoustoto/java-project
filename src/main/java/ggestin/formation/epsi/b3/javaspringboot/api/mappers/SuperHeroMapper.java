package ggestin.formation.epsi.b3.javaspringboot.api.mappers;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.PowerCreateDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.PowerDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.SuperHeroDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.SuperHeroMinimalDTO;
import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",
uses = {PowerMapper.class})
public interface SuperHeroMapper {

    @Mapping(target = "nameSuperHero", source = "name")
    SuperHeroMinimalDTO toMinimalDto(SuperHero entity);

    List<SuperHeroMinimalDTO> toMinimalDtos(List<SuperHero> entities);
}
